package ru.maxim.secondtask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.maxim.secondtask.dao.User;
import ru.maxim.secondtask.payroll.UserRepository;

@SpringBootApplication
public class SecondTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecondTaskApplication.class, args);
    }
}
