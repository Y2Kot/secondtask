package ru.maxim.secondtask.dao;

import lombok.Data;
import org.springframework.util.DigestUtils;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "Users")
@Data
@Entity
public class User implements Serializable {

    @Column(name = "id")
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "surname")
    private String surname;
    @Column(name = "name")
    private String name;
    @Column(name = "birthday")
    private String birthday;
    @Column(name = "password")
    private String password;
    @Column(name = "mail")
    private String mail;

    private User() {
    }
    
    public User(String surname, String name, String birthday, String password, String mail) {
        this.surname = surname;
        this.name = name;
        this.birthday = birthday;
        this.password = DigestUtils.md5DigestAsHex(password.getBytes());
        this.mail = mail;
    }
}
