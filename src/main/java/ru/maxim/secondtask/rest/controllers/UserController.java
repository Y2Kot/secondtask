package ru.maxim.secondtask.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxim.secondtask.dao.User;
import ru.maxim.secondtask.payroll.UserRepository;
import ru.maxim.secondtask.payroll.UsersResourceAssembler;
import ru.maxim.secondtask.rest.UserNotFoundException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    private final UserRepository repository;

    @Autowired
    private final UsersResourceAssembler assembler;

    public UserController(UserRepository repository, UsersResourceAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    // root
    @GetMapping("/users")
    public Resources<Resource<User>> all() {

        List<Resource<User>> users = repository.findAll().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(users,
                linkTo(methodOn(UserController.class).all()).withSelfRel());
    }

    @PostMapping("/users")
    public ResponseEntity<?> newUser(@RequestBody User newUser) throws URISyntaxException {
        Resource<User> resource = assembler.toResource(repository.save(newUser));

        return ResponseEntity.created(new URI(resource.getId().expand().getHref())).body(resource);
    }

    //single item
    @GetMapping("/users/{id}")
    public Resource<User> one(@PathVariable Long id) {
        User user = repository.findById(id).orElseThrow(() -> new UserNotFoundException(id));

        return assembler.toResource(user);
    }

    @PutMapping("/users/{id}")
    ResponseEntity<?> replaceUser(@RequestBody User newUser, @PathVariable Long id) throws URISyntaxException {
        User updateUser = repository.findById(id).map(user -> {
            user.setName(newUser.getName());
            user.setSurname(newUser.getSurname());
            user.setBirthday(newUser.getBirthday());
            user.setMail(newUser.getMail());
            user.setPassword(newUser.getPassword());
            return repository.save(user);
        }).orElseGet(() -> {
            newUser.setId(id);
            return repository.save(newUser);
        });
        Resource<User> resource = assembler.toResource(updateUser);

        return ResponseEntity.created(new URI(resource.getId().expand().getHref())).body(resource);
    }

    @DeleteMapping("/users/{id}")
    void deleteUser(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
