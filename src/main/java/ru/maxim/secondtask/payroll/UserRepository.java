package ru.maxim.secondtask.payroll;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxim.secondtask.dao.User;

public interface UserRepository extends JpaRepository<User, Long> {
}