package ru.maxim.secondtask.payroll;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.maxim.secondtask.dao.User;

@Configuration
public class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase (UserRepository userRepository) {
        return args -> {
            User user1 = new User(
                    "Kudryavtsev",
                    "Maxim",
                    "19.06.1994",
                    "password",
                    "kudryavtsevma@yandex.ru"
            );
            User user2 = new User(
                    "Kto",
                    "To",
                    "19.06.1849",
                    "qwerty",
                    "qwerty@to.ru"
            );
            User user3 = new User(
                    "Grot",
                    "Mc",
                    "13.02.884",
                    "topsecret",
                    "haha@lol.net"
            );
            userRepository.save(user1);
            userRepository.save(user2);
            userRepository.save(user3);
        };
    }
}
